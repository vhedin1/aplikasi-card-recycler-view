package com.example.aplikasicardrecyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class OSAdapter (private val listOS: ArrayList<OS>) : RecyclerView.Adapter<OSAdapter.CardViewViewHolder>() {
    inner class CardViewViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var imgPhoto: ImageView = itemView.findViewById(R.id.img_item_photo)
        var tvName: TextView = itemView.findViewById(R.id.tv_item_name)
        var tvInfo: TextView = itemView.findViewById(R.id.tv_item_info)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_card_os,parent,false)
        return CardViewViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOS.size
    }

    override fun onBindViewHolder(holder: CardViewViewHolder, position: Int) {
        val os = listOS[position]

        Glide.with(holder.itemView.context)
            .load(os.photo)
            .apply(RequestOptions())
            .into(holder.imgPhoto)

        holder.tvName.text = os.name
        holder.tvInfo.text = os.info

        holder.itemView.setOnClickListener {
            Toast.makeText(holder.itemView.context, "Anda memilih "+listOS[holder.absoluteAdapterPosition].name, Toast.LENGTH_SHORT).show()
        }
    }
}
package com.example.aplikasicardrecyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var rvOS: RecyclerView
    private val list = ArrayList<OS>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvOS = findViewById(R.id.rv_os)
        rvOS.setHasFixedSize(true)

        list.addAll(OSData.listData)
        showRecyclerView()
    }

    private fun showRecyclerView() {
        rvOS.layoutManager = LinearLayoutManager(this)
        val lisOSAdapter = OSAdapter(list)
        rvOS.adapter = lisOSAdapter
    }
}